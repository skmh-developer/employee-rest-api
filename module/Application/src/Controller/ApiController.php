<?php

namespace Application\Controller;

use Application\Service\ApiService;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

class ApiController extends AbstractActionController
{
    /**
     * @var ApiService
     */
    private $apiService;

    /**
     * ApiController constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->layout('layout/blank');
        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $type = $this->params()->fromRoute()['type'];
        $id   = isset($this->params()->fromRoute()['id']) ? $this->params()->fromRoute()['id'] : 0;

        if ($this->apiService->repositoryExist() === false) {
            return new JsonModel([
                'error' => 'API Endpoint "' . $type . '" does not exists.'
            ]);
        }

        /** @var Request $request */
        $request = $this->getRequest();

        $response = $this->apiService->processApiRequest($request, $type, $id);

        return new JsonModel($response);
    }
}
