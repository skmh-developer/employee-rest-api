<?php

namespace Application\Controller\Factory;

use Application\Controller\ApiController;
use Application\Service\ApiService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApiControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $apiService = $container->get(ApiService::class);
        return new ApiController($apiService);
    }
}
