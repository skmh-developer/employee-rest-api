<?php

namespace Application\Repository;

use Application\Entity\AbstractRestObject;
use DirectoryIterator;
use Application\Entity\Employee;

class EmployeeRepository implements RestObjectRepositoryInterface
{
    /**
     * @param AbstractRestObject $employee
     */
    public function save(AbstractRestObject $employee)
    {
        $id = $employee->getId();
        $filename = __DIR__ . '/data/' .$id.'.json';
        file_put_contents($filename, json_encode($employee->createRepresentation()));
    }

    /**
     * @param $id
     * @return AbstractRestObject | null
     */
    public function find($id)
    {
        $filename = __DIR__ . '/data/' .$id.'.json';
        $user = null;

        if (file_exists($filename)) {
            $user = new Employee();
            $representation = file_get_contents($filename);
            $user->populateObjectFromRepresentation(json_decode($representation, 1));
        }

        return $user;
    }

    public function findAll(): array
    {
        $iterator = new DirectoryIterator(__DIR__ . '/data/');
        $response = [];

        foreach ($iterator as $directory) {
            if (!$directory->isDot() && $directory->getExtension() === 'json') {
                $id =  $directory->getBasename('.json');
                $data = file_get_contents($directory->getPathname());

                $user = new Employee();
                $user->populateObjectFromRepresentation(json_decode($data, 1));

                $representation = $body = $user->createRepresentation();

                $representation['links']['GET'] = [
                    'rel'       => 'self',
                    'method'    => 'GET',
                    'link'      => 'http://localhost:3000/user/'.$id,
                    'userLevel' => 1
                ];

                $response[] = $representation;
            }
        }

        return $response;
    }

    /**
     * @param $id
     * @return bool
     */
    public function exists($id): bool
    {
        return file_exists(__DIR__ . '/data/' .$id.'.json');
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        $filename = __DIR__ . '/data/' .$id.'.json';

        if (file_exists($filename)) {
            unlink($filename);
            return true;
        }

        return false;
    }

    public function createNew() : AbstractRestObject
    {
        $newId = $this->createNewId();
        $employ = new Employee();
        $employ->setId($newId);

        return $employ;
    }

    public function createNewId(int $id = 1): int
    {
        if (file_exists(__DIR__ . '/data/' .$id.'.json')) {
            $id++;
            $id = self::createNewId($id);
        }

        return $id;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function checkIfEmailExists(string $email): bool
    {
        $allUsers = $this->findAll();

        foreach ($allUsers as $user) {
            if ($user['email'] === $email) {
                return true;
            }
        }

        return false;
    }
}
