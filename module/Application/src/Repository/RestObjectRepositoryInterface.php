<?php
namespace Application\Repository;

use Application\Entity\AbstractRestObject;

interface RestObjectRepositoryInterface
{
    public function save(AbstractRestObject $abstractRestObject);

    /**
     * @param $id
     * @return AbstractRestObject | null
     */
    public function find($id);

    public function findAll(): array;

    public function delete($id);

    public function createNew(): AbstractRestObject;

    public function exists($id);

    public function checkIfEmailExists(string $email): bool;
}
