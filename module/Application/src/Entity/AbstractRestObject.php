<?php

namespace Application\Entity;

abstract class AbstractRestObject
{
    /** @var int */
    protected $id;

    /**
     * @param array $data
     * @return array
     */
    public function populateObjectFromRepresentation(array $data): array
    {
        $errors = [];
        $attributes = get_object_vars($this);

        foreach ($attributes as $attribute => $value) {
            if (!isset($data[$attribute])) {
                $errors[$attribute] = 'Object incomplete. Attribute '.$attribute.' is not given';
                continue;
            }
            $this->$attribute = $data[$attribute];
        }

        return $errors;
    }

    public function createRepresentation(): array
    {
        $attributes = get_object_vars($this);
        $representation = [];

        foreach ($attributes as $attribute => $value) {
            $representation[$attribute] = $value;
        }

        return $representation;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
