<?php
namespace Application\Service\Request;

use Application\Repository\RestObjectRepositoryInterface;
use Zend\Http\PhpEnvironment\Request;

class GetService implements MethodServiceInterface
{
    /**
     * @param Request $request
     * @param RestObjectRepositoryInterface $repository
     * @param $type
     * @param $id
     * @param $userLevel
     *
     * @return array
     */
    public function processMethod(Request $request, RestObjectRepositoryInterface $repository, $type, $id, $userLevel)
    {
        if ($id === 'all') {
            $response = $repository->findAll();
            $response[]['success'] = 'true';
        } else {
            $restObject = $repository->find($id);

            if ($restObject === null) {
                return [
                    'method'  => 'GET',
                    'error'  => [
                        '404' => 'not found'
                    ],
                    'userLevel' => $userLevel,
                    'success' => false,
                ];
            }

            $response   = $representation = $restObject->createRepresentation();

            $response['links']['DELETE'] = [
                'rel'    => 'self',
                'method' => 'DELETE',
                'link'   => 'http://localhost:3000/'.$type.'/'.$id,
            ];
            $response['links']['PUT'] = [
                'rel'    => 'self',
                'method' => 'PUT',
                'link'   => 'http://localhost:3000/'.$type.'/'.$id,
                'body'   => $representation,
            ];
            $response['method']  = 'GET';
            $response['success'] = 'true';
            $response['userLevel'] = $userLevel;
        }

        return $response;
    }
}
