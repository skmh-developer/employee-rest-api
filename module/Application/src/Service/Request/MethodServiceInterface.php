<?php
namespace Application\Service\Request;


use Application\Repository\RestObjectRepositoryInterface;
use Zend\Http\PhpEnvironment\Request;

interface MethodServiceInterface
{
    public function processMethod(Request $request, RestObjectRepositoryInterface $repository, $type, $id, $userLevel);
}
