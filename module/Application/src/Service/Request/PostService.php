<?php
namespace Application\Service\Request;

use Application\Repository\RestObjectRepositoryInterface;
use Zend\Http\PhpEnvironment\Request;

class PostService implements MethodServiceInterface
{
    public function processMethod(Request $request, RestObjectRepositoryInterface $repository, $type, $id, $userLevel)
    {
        if ($userLevel > 3) {
            return [
                'method'  => 'POST',
                'error'  => [
                    '401' => 'no permission to post'
                ],
                'userLevel' => $userLevel,
                'success' => false
            ];
        }

        $representation = json_decode($request->getContent(), 1);

        if (!isset($representation['email']) || empty($representation['email'])) {
            return [
                'method'  => 'POST',
                'errors'  => 'E-Mail-Adress expired',
                'userLevel' => $userLevel,
                'success' => false,
            ];
        }

        if ($repository->checkIfEmailExists($representation['email'])) {
            return [
                'method'  => 'POST',
                'errors'  => 'E-Mail-Adress allready exists',
                'userLevel' => $userLevel,
                'success' => false,
            ];
        }

        $restObject = $repository->createNew();
        $representation['id'] = $restObject->getId();
        $errors = $restObject->populateObjectFromRepresentation($representation);

        if (count($errors) > 0) {
            return [
                'method'  => 'POST',
                'errors'  => $errors,
                'userLevel' => $userLevel,
                'success' => false,
            ];
        }

        $repository->save($restObject);

        return [
            'method'    => 'POST',
            'userLevel' => $userLevel,
            'success'   => true
        ];
    }
}
