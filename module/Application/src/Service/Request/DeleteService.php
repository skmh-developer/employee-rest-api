<?php
namespace Application\Service\Request;

use Application\Repository\RestObjectRepositoryInterface;
use Zend\Http\PhpEnvironment\Request;

class DeleteService implements MethodServiceInterface
{
    public function processMethod(Request $request, RestObjectRepositoryInterface $repository, $type, $id, $userLevel)
    {
        if ($userLevel > 1) {
            return [
                'method'  => 'DELETE',
                'error'  => [
                    '401' => 'no permission to delete'
                ],
                'userLevel' => $userLevel,
                'success' => false
            ];
        }

        $restObject = $repository->find($id);

        if ($restObject === null) {
            return [
                'method'  => 'DELETE',
                'error'  => [
                    '404' => 'not found'
                ],
                'userLevel' => $userLevel,
                'success' => false,
            ];
        }

        $deleted = $repository->delete($id);

        return [
            'method'    => 'DELETE',
            'userLevel' => $userLevel,
            'success'   => $deleted,
        ];
    }
}
