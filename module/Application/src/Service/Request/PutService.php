<?php
namespace Application\Service\Request;

use Application\Repository\RestObjectRepositoryInterface;
use Zend\Http\PhpEnvironment\Request;

class PutService implements MethodServiceInterface
{
    public function processMethod(Request $request, RestObjectRepositoryInterface $repository, $type, $id, $userLevel)
    {
        if ($userLevel > 2) {
            return [
                'method'  => 'PUT',
                'error'  => [
                    '401' => 'no permission to put'
                ],
                'userLevel' => $userLevel,
                'success' => false
            ];
        }

        $representation = json_decode($request->getContent(), 1);

        if (!isset($representation['email']) || empty($representation['email'])) {
            return [
                'method'  => 'PUT',
                'errors'  => 'E-Mail-Adress expired',
                'userLevel' => $userLevel,
                'success' => false,
            ];
        }

        $restObject = $repository->find($id);

        if ($restObject === null) {
            return [
                'method'    => 'PUT',
                'error'   => [
                    '404' => 'not found'
                ],
                'userLevel' => $userLevel,
                'success'   => false
            ];
        }

        $representation['id'] = $id;
        $errors = $restObject->populateObjectFromRepresentation($representation);

        if (count($errors) > 0) {
            return [
                'method'  => 'PUT',
                'errors'  => $errors,
                'userLevel' => $userLevel,
                'success' => false,
            ];
        }

        $restObject->setId($id);

        $repository->save($restObject);

        return [
            'method'    => 'PUT',
            'userLevel' => $userLevel,
            'success'   => true,
        ];
    }
}
