<?php
namespace Application\Service;

use Application\Repository\RestObjectRepositoryInterface;
use Application\Service\Request\MethodServiceInterface;
use Zend\Http\PhpEnvironment\Request;

class ApiService
{
    /**
     * UserLevel 1 : get, put, post, delete
     * UserLevel 2 : get, put, post
     * UserLevel 3 : get, post
     * UserLevel 4 : get
     */
    const API_KEYS = [
        'FkhHOzpqMItScN9bEqLp6WVVGCOgJwXbn2FaRBQFywixkqstvlqLENPNufa43rj' => 1,
        'zKDyieuYBbt2xcQoxZ5aCZxDjPfNdoFM3HRD96rGQ5bypfXKAKb01iXkKdK36Cw' => 2,
        'Z5aCZxDjPfNdoFM3HRD96rGQ5bypxcQoxKAKb01iXkKdK36CwfXzKDyieuYBbt2' => 3,
        'qstvlqLENPNufa43rjFkhHOzpqMItScN9bEqLp6WVVGCOgJwXbn2FaRBQFywixk' => 4
    ];

    /** @var MethodServiceInterface */
    private $methodService;

    /** @var RestObjectRepositoryInterface|null  */
    private $repository;

    /**
     * ApiService constructor.
     * @param RestObjectRepositoryInterface|null $repository
     * @param MethodServiceInterface $methodService
     */
    public function __construct($repository, MethodServiceInterface $methodService)
    {
        $this->repository     = $repository;
        $this->methodService  = $methodService;
    }

    public function repositoryExist(): bool
    {
        return $this->repository instanceof RestObjectRepositoryInterface;
    }

    /**
     * @param Request $request
     * @param $type
     * @param $id
     * @return array
     */
    public function processApiRequest(Request $request, $type, $id): array
    {
        $apiKeyHeader = $request->getHeader('Apikey');

        if (!$apiKeyHeader) {
            return [
                'error' => 'authorization failed',
                'apiKey' => 'no key',
            ];
        }

        $apiKey = $apiKeyHeader->getFieldValue();

        if (!isset(self::API_KEYS[$apiKey])) {
            return [
                'error' => 'authorization fail',
                'apiKey' => $apiKey,
            ];
        }

        $userLevel = self::API_KEYS[$apiKey];

        return $this->methodService->processMethod($request, $this->repository, $type, $id, $userLevel);
    }
}
