<?php

namespace Application\Service\Factory;

use Application\Service\ApiService;
use Application\Service\Request\OptionsService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Interop\Container\ContainerInterface;
use Synatix\Employee\Repository\UserRepository;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Uri\Http;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ApiServiceFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return ApiService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ApiService
    {
        /** @var Request $request */
        $request = $container->get('Request');


        $method      = $request->getMethod();
        $serviceName = 'Application\\Service\\Request\\'.ucwords(strtolower($method)).'Service';
        $service     = $container->get($serviceName);

        $moduleName = ucwords(strtolower(explode('/', $request->getRequestUri())[1]));

        if ($moduleName === 'User' || $moduleName === 'Employee') {
            $moduleName = 'Employee';
        }

        $repoName   = 'Application\\Repository\\' . $moduleName . 'Repository';

        if ($container->has($repoName) === false) {
            return new ApiService(null, $service);
        }

        $repository = $container->get($repoName);

        return new ApiService($repository, $service);
    }
}
