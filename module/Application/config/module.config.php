<?php
namespace Application;

use Application\Controller\ApiController;
use Application\Controller\Factory\ApiControllerFactory;
use Application\Service\ApiService;
use Application\Service\Factory\ApiServiceFactory;
use Application\Service\Request\DeleteService;
use Application\Service\Request\GetService;
use Application\Service\Request\PostService;
use Application\Service\Request\PutService;
use Application\Repository\EmployeeRepository;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Method;
use Zend\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            ApiController::class => ApiControllerFactory::class
        ],
        'invokables' => [
        ],
    ],
    'service_manager' => [
        'factories' => [
            ApiService::class => ApiServiceFactory::class,
        ],
        'invokables' => [
            DeleteService::class  => DeleteService::class,
            PostService::class    => PostService::class,
            GetService::class     => GetService::class,
            PutService::class     => PutService::class,
            EmployeeRepository::class => EmployeeRepository::class
        ],
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => ApiController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'get' => [
                        'type'    => Method::class,
                        'options' => ['verb' => 'GET'],
                        'may_terminate' => true,
                        'child_routes' => [
                            'get' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'       => '[:type][/:id]',
                                    'defaults' => [
                                        'controller' => ApiController::class,
                                        'action' => 'index'
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'post' => [
                        'type'    => Method::class,
                        'options' => ['verb' => 'POST'],
                        'may_terminate' => true,
                        'child_routes' => [
                            'post' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:type][/:id]',
                                    'defaults' => [
                                        'controller' => ApiController::class,
                                        'action' => 'index'
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'put' => [
                        'type'    => Method::class,
                        'options' => ['verb' => 'PUT'],
                        'may_terminate' => true,
                        'child_routes' => [
                            'put' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:type][/:id]',
                                    'defaults' => [
                                        'controller' => ApiController::class,
                                        'action' => 'index'
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'delete' => [
                        'type'    => Method::class,
                        'options' => ['verb' => 'DELETE'],
                        'may_terminate' => true,
                        'child_routes' => [
                            'delete' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:type][/:id]',
                                    'defaults' => [
                                        'controller' => ApiController::class,
                                        'action' => 'index'
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];
