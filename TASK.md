# CodeFrog IT GmbH - PHP Testaufgabe

### Aufgabe “Mitarbeiterverwaltung”

Erstellen Sie eine REST-API zur Verwaltung von Mitarbeitern mithilfe der Programmiersprache PHP (Version 7) und senden 
Sie das Projekt bitte als .zip-Datei an uns zurück. Die Anwendung soll auf dem Objekt Employee basieren und die genannten 
Methoden implementieren bzw. als Schnittstelle anbieten. Die Daten sollen im JSON-Format übergeben werden. 

### Gegeben: Employee
- id (Integer)
- email (String)
- name (String)
- surname (String)
- birthday (Date)

### Gesucht:

|Controler | Rückgabe | Fehler |
|:---|---|---|
|GET /user/all|gibt die Liste aller Benutzer zurück|---|
|GET /user/{id}|gibt einen bestimmten Benutzer zurück|404 : Not Found|
|POST /user|fügt dem System einen Benutzer hinzu|409 : Conflict / - Email vorhanden|
|PUT /user/{id}|passt den Benutzer mit einer bestimmten ID an|404 : Not Found|
|DELETE /user/{id}|entfernt Benutzer mit einer bestimmten ID|404 : Not Found|

# Hinweise 
>   • Sie dürfen ein Framework ihrer Wahl verwenden (Zend, Synfony, Laravel, ...)
    • Den Quellcode bitte als ZIP-Archiv beilegen.
    • Geben Sie zudem bitte an, wieviel Zeit Sie (ungefähr) für diese Aufgabe benötigt haben.
