########################################################################################################################
# Stage: base
########################################################################################################################
FROM php:7.0-apache as base

COPY ./ /var/www/

WORKDIR /var/www

RUN apt-get update \
 && apt-get install -y git zlib1g-dev libzip-dev inotify-tools wget gpg sudo vim \
 && rm -Rf /var/lib/apt/lists/* \
 && docker-php-ext-install zip pdo pdo_mysql \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && echo 'AllowEncodedSlashes On' >> /etc/apache2/apache2.conf \
 && a2enmod rewrite

RUN apt-get update \
 && apt-get install -y wget \
 && rm -rf /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
 && chmod +x /usr/local/bin/composer

RUN { \
        echo "alias ls='ls --color=auto'"; \
        echo "alias ll='ls --color=auto -alF'"; \
        echo "alias la='ls --color=auto -A'"; \
        echo "alias lc='ls --color=auto -CF'"; \
        echo "alias l='ls --color=auto -lah'"; \
    } >> ~/.bashrc

RUN useradd -ms /bin/bash docker && adduser docker sudo
#Users in the sudoers group can sudo as root without password.
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN cd /var/www \
 && composer install \
 && chown -Rf www-data:www-data /var/www/
