# Employee Rest API - PHP

## Run API Endpoint Employee-Application

Startup the stack via docker:

```
$ docker-compose up -d --build
```

get in docker container
```
$ docker container exec -it employee-rest-api /bin/bash
```

## Check the Endpoints with Postman

Import the 'employee.postman.collection.json' into your Postman Apllication

